namespace FsHackerRank

module HelloWorldNTimes =

    open System

    let timesToPrint = Console.ReadLine() |> int

    let printHelloWorld number = 
        for i = 1 to number do
            printfn "Hello World"

    printHelloWorld timesToPrint