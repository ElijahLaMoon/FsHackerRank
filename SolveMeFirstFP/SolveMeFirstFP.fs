﻿namespace FsHackerRank

module SolveMeFirstFP =

    open System

    let argumentA = 
        //printf "Enter a: "
        let a = Console.ReadLine() |> int
        match a with
        | a when a >= 1 && a <= 1000 -> a
        | _ -> failwith "value must be greater or equal to 1, and lesser or equal to 1000"

    let argumentB =
        //printf "Enter b: "
        let b = Console.ReadLine() |> int
        match b with
        | b when b >= 1 && b <= 1000 -> b
        | _ -> failwith "value must be greater or equal to 1, and lesser or equal to 1000"

    printfn "%d" (argumentA + argumentB)